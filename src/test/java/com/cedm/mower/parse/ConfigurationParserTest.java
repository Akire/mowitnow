package com.cedm.mower.parse;

import com.cedm.mower.ParseFileException;
import com.cedm.mower.model.Configurations;
import com.cedm.mower.model.LawnSize;
import com.cedm.mower.model.MowerConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationParserTest {

    @InjectMocks
    private ConfigurationParser tested;

    @Mock
    private MowerConfigurationsMapper mowerConfigurationsMapper;

    @Mock
    private LawnMapper lawnMapper;

    @Test(expected = ParseFileException.class)
    public void testParseThrowErrorWhenFileNotExist() throws Exception {
        tested.parse("fakeFile");
    }


    @Test
    public void testParse() throws Exception {
        List<List<String>> groupedConfigurationList = new ArrayList<>();
        groupedConfigurationList.add(Arrays.asList("6 6", "3 3N", "GDA"));
        List<MowerConfiguration> expectedMowerConfigurations = new ArrayList<>();
        expectedMowerConfigurations.contains(mock(MowerConfiguration.class));
        when(mowerConfigurationsMapper.map(groupedConfigurationList)).thenReturn(expectedMowerConfigurations);
        LawnSize expectedLawnSize = mock(LawnSize.class);
        when(lawnMapper.map("6 6")).thenReturn(expectedLawnSize);

        Configurations actual = tested.parse("target/test-classes/MowerTest.txt");

        assertThat(actual.getMowerConfigurations(), is(expectedMowerConfigurations));
        assertThat(actual.getLawnSize(), is(expectedLawnSize));
    }


}