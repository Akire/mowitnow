package com.cedm.mower;

import com.cedm.mower.model.*;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ControlServiceTest {

    @InjectMocks
    private ControlService service;

    @Mock
    private MowerFactory factory;

    @Test
    public void testName() throws Exception {
        LawnSize lawnSize = mock(LawnSize.class);
        List<MowerConfiguration> list = new ArrayList<>();
        Position postion = mock(Position.class);
        List<Order> orders = mock(List.class);
        MowerConfiguration mowerConfiguration = new MowerConfiguration(postion, orders);
        list.add(mowerConfiguration);
        Configurations configurations = new Configurations(lawnSize, list);
        Mower mower = mock(Mower.class);
        when(factory.create(postion, lawnSize)).thenReturn(mower);
        Position expectedPosition = mock(Position.class);
        when(mower.executeOrders(orders)).thenReturn(expectedPosition);

        List<Position> actual = service.pilot(configurations);


        assertThat(actual, is(Lists.newArrayList(expectedPosition)));
    }
}