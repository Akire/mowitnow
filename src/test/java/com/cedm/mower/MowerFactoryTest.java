package com.cedm.mower;

import com.cedm.mower.model.Direction;
import com.cedm.mower.model.LawnSize;
import com.cedm.mower.model.Mower;
import com.cedm.mower.model.Position;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class )
public class MowerFactoryTest {

    @InjectMocks
    private MowerFactory tested;

    @Test
    public void testCreate() throws Exception {
        Position position = mock(Position.class);
        Mower mower = tested.create(position, new LawnSize(10, 11));

        assertThat(mower.getMaxWidth(), is(10));
        assertThat(mower.getMaxHeight(), is(11));
        assertThat(mower.getPosition(), is(position));
    }
}