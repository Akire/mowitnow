package com.cedm.mower;

import com.cedm.mower.app.configuration.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;


public class IntegrationTest {


    @org.junit.Test
    public void testName() throws Exception {
        LaunchMows bean = initialiazeBean();

        bean.start(new String[]{"target/test-classes/MowerTest.txt"});
    }

    private LaunchMows initialiazeBean() {
        ApplicationContext applicationContext = SpringApplication.run(Config.class);
        return applicationContext.getBean(LaunchMows.class);
    }
}