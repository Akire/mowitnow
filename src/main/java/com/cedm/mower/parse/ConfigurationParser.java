package com.cedm.mower.parse;

import com.cedm.mower.ParseFileException;
import com.cedm.mower.model.Configurations;
import com.cedm.mower.model.LawnSize;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ConfigurationParser {

    private static final int MOWER_CONFIGURATIONS_LINES_CONFIG = 2;

    @Autowired
    private MowerConfigurationsMapper mowerConfigurationsMapper;

    @Autowired
    private LawnMapper lawnMapper;

    public Configurations parse(final String path) throws Exception {
        try (Stream<String> stream = Files.lines(Paths.get(path))) {
            final List<String> lines = fileToLines(stream);
            final LawnSize lawnSize = lawnMapper.map(lines.get(0));
            final List<String> mowerConfigurationLines = removeLawnConfigurationLine(lines);
            List<List<String>> mowersConfigurationGrouped = groupConfigurationByMower(mowerConfigurationLines);
            return new Configurations(lawnSize, mowerConfigurationsMapper.map(mowersConfigurationGrouped));

        } catch (Exception e) {
            throw new ParseFileException(e.getMessage());
        }
    }

    private List<String> fileToLines(Stream<String> stream) {
        return stream
                .filter(isNotBlankOrEmpty())
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private List<String> removeLawnConfigurationLine(List<String> lines) {
        return lines.subList(1, lines.size());
    }

    private List<List<String>> groupConfigurationByMower(List<String> collect) {
        return Lists.partition(collect, MOWER_CONFIGURATIONS_LINES_CONFIG);
    }

    private Predicate<String> isNotBlankOrEmpty() {
        return s -> !s.trim().isEmpty();
    }
}
