package com.cedm.mower.parse;

import com.cedm.mower.model.LawnSize;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class LawnMapper {

    private final static String LAWN_SIZE_PATTERN = "[0-9]+ [0-9]+";

    public LawnSize map(String lawnLine) {
        if(!Pattern.matches(LAWN_SIZE_PATTERN, lawnLine)) {
            throw new IllegalArgumentException("Illegal argument for lawn size: " + lawnLine);
        }
        final String[] splitted = lawnLine.split(" ");
        return new LawnSize(Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1]));
    }
}
