package com.cedm.mower;

import com.cedm.mower.model.Configurations;
import com.cedm.mower.model.Mower;
import com.cedm.mower.model.MowerConfiguration;
import com.cedm.mower.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class ControlService {
    @Autowired
    private MowerFactory factory;

    public List<Position> pilot(Configurations configuration) {
        final List<Position> positions = new ArrayList<>();
        for (MowerConfiguration mowerConfiguration : configuration.getMowerConfigurations()) {
            Mower mower = factory.create(mowerConfiguration.getPosition(), configuration.getLawnSize());
            positions.add(mower.executeOrders(mowerConfiguration.getOrders()));
        }
        return positions;
    }
}
