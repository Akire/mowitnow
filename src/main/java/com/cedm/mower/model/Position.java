package com.cedm.mower.model;

import static com.cedm.mower.model.Direction.*;

public class Position {

    private Integer x;
    private Integer y;
    private Direction direction;

    public Position(Integer x, Integer y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Position defineNextPosition() {
        if (NORTH.equals(direction)) {
            return new Position(x, y + 1, direction);
        } else if (SOUTH.equals(direction)) {
            return new Position(x, y - 1, direction);
        } else if (WEST.equals(direction)) {
            return new Position(x - 1, y, direction);
        } else if(EAST.equals(direction)){
            return new Position(x + 1, y, direction);
        }
        throw new IllegalArgumentException();
    }
}
