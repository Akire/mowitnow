package com.cedm.mower.model;

public class LawnSize {

    private final Integer width;
    private final Integer height;

    public LawnSize(Integer width, Integer height) {
        this.width = width;
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }
}
