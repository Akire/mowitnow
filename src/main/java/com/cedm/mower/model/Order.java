package com.cedm.mower.model;

public enum Order {
    GAUCHE,
    DROITE,
    AVANCE;

    public static Order charToAction(Character c) {
        switch (c) {
            case 'G':
                return GAUCHE;
            case 'D':
                return DROITE;
            case 'A':
                return AVANCE;
            default:
                throw new IllegalArgumentException("The following character is not allowed for order: " + c);
        }
    }
}
