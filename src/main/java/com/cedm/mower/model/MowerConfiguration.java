package com.cedm.mower.model;

import java.util.List;

public class MowerConfiguration {

    private final Position position;
    private final List<Order> orders;

    public MowerConfiguration(Position position, List<Order> orders) {
        this.position = position;
        this.orders = orders;
    }


    public Position getPosition() {
        return position;
    }

    public List<Order> getOrders() {
        return orders;
    }
}
