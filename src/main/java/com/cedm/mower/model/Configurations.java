package com.cedm.mower.model;

import java.util.List;

public class Configurations {

    private final LawnSize lawnSize;
    private final List<MowerConfiguration> mowerConfigurations;

    public Configurations(LawnSize lawnSize, List<MowerConfiguration> mowerConfigurations) {
        this.lawnSize = lawnSize;
        this.mowerConfigurations = mowerConfigurations;
    }

    public LawnSize getLawnSize() {
        return lawnSize;
    }

    public List<MowerConfiguration> getMowerConfigurations() {
        return mowerConfigurations;
    }
}
