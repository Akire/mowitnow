package com.cedm.mower.exception;

public class PathNotFoundException extends RuntimeException {

    public PathNotFoundException(String s) {
        super(s);
    }
}
