package com.cedm.mower;

import com.cedm.mower.exception.PathNotFoundException;
import com.cedm.mower.model.Configurations;
import com.cedm.mower.model.Position;
import com.cedm.mower.parse.ConfigurationParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LaunchMows {

    @Autowired
    private ControlService controlService;

    @Autowired
    private ConfigurationParser parser;

    public void start(final String[] args) throws Exception{
        if(doesNotContainArg(args)) {
            throw new PathNotFoundException("No path exception found. First argument");
        }
        final Configurations configurations = parser.parse(args[0]);
        List<Position> mowerPositions = controlService.pilot(configurations);

        mowerPositions.stream().forEach(this::printDirection);
    }

    private void printDirection(Position s) {
        System.out.println(s.getX() + " " + s.getY() + " " + s.getDirection().getCode());
    }

    private boolean doesNotContainArg(String[] args) {
        return args == null || args.length == 0;
    }
}
