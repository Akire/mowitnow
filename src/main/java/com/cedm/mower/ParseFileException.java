package com.cedm.mower;

public class ParseFileException extends RuntimeException {
    public ParseFileException(String s) {
        super(s);
    }
}
